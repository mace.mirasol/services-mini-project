'use strict';
const {
  get_place_ids,
  get_coordinates,
  get_suggestions,
  filter_suggestions
} = require('./getRequests');

module.exports = async ({ query }) => {
  // Storing results of get_place_ids to use for coordinates
  console.log(query.province, query.municipality, query.barangay);

  const place_ids = await get_place_ids(
    query.province,
    query.municipality,
    query.barangay
  );

  // Storing results of coordinates to use for suggestions
  const coordinates = await get_coordinates(place_ids);

  // Storing results of suggestions to use for cleanedSuggestions
  const suggestions = await get_suggestions(coordinates, query.searchText);

  // Final results are stored in this array
  const filtered_suggestions = await filter_suggestions(
    suggestions,
    query.province
  );

  // Uncomment the code below and comment out -
  // the conditional statement at the end of the try block -
  // if you wish to see the unfiltered suggestions

  // res.send(suggestions);

  let data =
    suggestions.length === 0
      ? [{ address: 'No results found. Please check your input.' }]
      : filtered_suggestions;

  return data;
};
