'use strict';

const ProvinceModel = require('../../models').ProvinceModel;
const MunicipalityModel = require('../../models').MunicipalityModel;
const BarangayModel = require('../../models').BarangayModel;

// The models are stored in this array to be used in the function
const models = [ProvinceModel, MunicipalityModel, BarangayModel];

module.exports = async (input, model_index) => {
  return new Promise(resolve => {
    resolve(models[model_index].find({ name: input }));
  });
};
