'use strict';

const getLocationQuery = require('./getLocationQuery');
const getAllQuery = require('./getAllQuery');

module.exports = async ({ query }) => {
  // This is the variable we return
  // It will contain an array of:
  // provinces/municipalities/barangays
  // by the end of this function unless an error is caught
  let data;

  // If there is NO PROVINCE/MUNICIPALITY in the input,
  // this block will display all provinces in the Philippines

  // This retrieves all the provinces from mongodb via 'getAllQuery'
  const getProvinces = await getAllQuery('R443174', 0);

  // This array will store the province names
  const provinces = [];

  // Store only the names of the retrieved provinces in the array
  getProvinces.forEach(province => {
    provinces.push(province.name);
  });

  // 'data' is now the 'provinces' array
  data = provinces;

  // If there is a PROVINCE in the input,
  // this block will display all the municipalities in that province
  if (query.province) {
    // This retrieves the province from mongodb via 'getLocationQuery'
    const getProvince = await getLocationQuery(query.province, 0);

    // If no such province is located, an error message is thrown
    if (getProvince.length === 0)
      throw `${query.province} does not exist in the Philippines.`;

    // This retrieves all the municipialities from mongodb via 'getAllQuery'
    const getMunicipalities = await getAllQuery(getProvince[0].id, 1);

    // This array will store the municipality names
    const municipalities = [];
    let inProvince = false;

    // Store only the names of the retrieved municipalities in the array
    getMunicipalities.forEach(municipality => {
      // This checks if there is a municipality in the input
      // If there is, its existence in the province is checked
      // If it exists, 'inProvince' will be set to true
      if (query.municipality && municipality.name === query.municipality)
        inProvince = true;
      municipalities.push(municipality.name);
    });

    // 'data' is now the 'municipalities' array
    data = municipalities;

    // If there is both PROVINCE and MUNICIPALITY in the input,
    // this block will display all the barangays in that municiaplity
    if (query.municipality) {
      // This retrieves the municipality from mongodb via 'getLocationQuery'
      const getMunicipality = await getLocationQuery(query.municipality, 1);

      // In the case that the municipality is not within the scope of
      // the province or does not exist, an error message is thrown
      // or if no such municipality is located, an error message is thrown
      if (!inProvince || getMunicipality.length === 0)
        throw `${query.municipality} does not exist in ${query.province}.`;

      // This retrieves the barangays from mongodb via 'getAllQuery'
      const getBarangays = await getAllQuery(getMunicipality[0].id, 2);

      // If no barangays are retrieved, an error message is thrown
      if (getBarangays.length === 0)
        throw `There are no barangays recorded for ${query.municipality}.`;

      // This array will store the barangay names
      const barangays = [];

      // Store only the names of the retrieved barangays in the array
      getBarangays.forEach(barangay => {
        barangays.push(barangay.name);
      });

      // 'data' is now the 'barangays' array
      data = barangays;
    }
  }

  return data;
};
