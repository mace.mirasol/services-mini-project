'use strict';

module.exports = (mongoose, Schema) => {
  const BarangaySchema = new Schema({
    id: { type: String, required: true },
    name: { type: String, required: true },
    parentId: { type: String, required: true }
  });

  return mongoose.model('barangays', BarangaySchema);
};
