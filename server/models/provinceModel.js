'use strict';

module.exports = (mongoose, Schema) => {
  const ProvinceSchema = new Schema({
    id: { type: String, required: true },
    name: { type: String, required: true },
    parentId: { type: String, required: true }
  });

  return mongoose.model('provinces', ProvinceSchema);
};
