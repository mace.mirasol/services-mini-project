'use strict';

// Import the controller files here
const ApiRequest = require('./ApiRequest');
const MapApiController = require('./MapApiController');
// Consolidate and export them as part of the `SampleController`
module.exports = {
  ApiRequest,
  MapApiController
};
