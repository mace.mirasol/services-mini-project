'use strict';

// Import the main utility file that will handle the processing logic for this
// controller file
const ApiRequest = require('../../utils/ApiRequest');

// Most of the surface level checkers should be contained w/in the controller
// file, so as to relieve the utility file of initial checking for variable
// existence and other easily checked user inputs
module.exports = (req, res, next) => {
  // Wrap async function in promise to catch all errors inside async function,
  // even without try-catch blocks
  ApiRequest({ query: req.query })
    .then(response => {
      if (!req.query.province && req.query.municipality)
        throw `Please include a province when searching for barangays via municipality`;

      req.responseData = {
        statusCode: 200,
        body: response
      };
      // Go to next middleware
      return next();
    })
    .catch(err => {
      req.responseData = {
        statusCode: err.statusCode || 404,
        body: 'error : ' + err || err
      };
      // Go to next middleware
      return next();
    });
  // }
};
