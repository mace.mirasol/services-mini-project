'use strict';

const MapApiRequest = require('../../utils/MapApiRequest');

module.exports = (req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );

  MapApiRequest({ query: req.query })
    .then(response => {
      if (!req.query.province && req.query.municipality && req.query.barangay)
        throw `Please include a province when searching for barangays via municipality`;

      req.responseData = {
        statusCode: 200,
        body: response
      };
      // Go to next middleware
      return next();
    })
    .catch(err => {
      req.responseData = {
        statusCode: err.statusCode || 404,
        body: 'error : ' + err || err
      };
      // Go to next middleware
      return next();
    });
};
